import os
import cftime
# import hydrostats.visual as hv
import matplotlib.pyplot as plt
import numpy as np
import numpy.ma as ma
import pandas as pd
import xarray as xr
# from ewatercycle.parametersetdb import build_from_urls
# from ewatercycle.observation.grdc import get_grdc_data
# from grpc4bmi.bmi_client_docker import BmiClientDocker

# Start and end dates
dstart = '1990-01-01'
dend = '2003-12-31'

# Load the configuration file
# I don't have the ewatercycle module
# parameter_set = build_from_urls(
#     config_format='yaml', config_url='https://bitbucket.org/dalmo1991/hbv_fair_workshop/raw/c3c3f8c1dac6d1d903d214e965d77bd0ecba96cd/config.yml',
#     datafiles_format='symlink', datafiles_url='', 
# )
import yaml
config_file = '/home/dalmo/Documents/BitBucket/hbv_fair_workshop/config.yml'
with open(config_file, 'r') as file:
    parameter_set = yaml.load(file, Loader = yaml.FullLoader)

# Old version with ewatercycle
# parameter_set.config['forcings']['precipitation'] = 'input/meuse_era5_total_precipitation.nc'
# parameter_set.config['forcings']['potential_evapotranspiration'] = 'input/meuse_inverted_era5_potential_evaporation.nc'
# parameter_set.config['time_step_unit'] = 'days since 1990-01-01 00:00:00'
# parameter_set.config['start_date'] = dstart
# parameter_set.config['end_date'] = dend
# parameter_set.save_config('input/config.yml')


# New version with yaml
parameter_set['forcings']['precipitation'] = 'input/meuse_era5_total_precipitation.nc'
parameter_set['forcings']['potential_evapotranspiration'] = 'input/meuse_inverted_era5_potential_evaporation.nc'
parameter_set['time_step_unit'] = 'days since 1990-01-01 00:00:00'
parameter_set['start_date'] = dstart
parameter_set['end_date'] = dend

# Save the updated file
with open('input/config.yml', 'w') as outfile:
    yaml.dump(data = parameter_set, stream = outfile)


# Check if inputs make sense before running everything
# precipitation_pointer = xr.open_dataset(parameter_set.config['forcings']['precipitation']).sel(time=pd.date_range(dstart, dend))
# pet_pointer = xr.open_dataset(parameter_set.config['forcings']['potential_evapotranspiration']).sel(time=pd.date_range(dstart, dend))
precipitation_pointer = xr.open_dataset(parameter_set['forcings']['precipitation']).sel(time=pd.date_range(dstart, dend))
pet_pointer = xr.open_dataset(parameter_set['forcings']['potential_evapotranspiration']).sel(time=pd.date_range(dstart, dend))

# fig, ax = plt.subplots(1, 1)
# ax.plot(precipitation_pointer.tp[0])
# plt.show(block = False)

# fig, ax = plt.subplots(1, 1)
# ax.plot(pet_pointer.pev[0])
# plt.show(block = False)

# The data make sense but keep in mind that they are meters but the model works in mm. The transformation is handled in the BMI interface, inside the update method

# Calibration of the model
# Station 6421500, MAAS BORGHAREN. We assume that this is the station associated with the shapefile "meuse_hydrosheds.shp" that was used to cut the data. **Can someone please check?**
# Warm-up period: 01-Jan-1990 -> 31-Dec-1991
# Calibration period: 01-Jan-1992 -> 31-Dec-2003

from hbv.HBV_bmi_spotpy import spotpy_setup
import spotpy

# # Get the data
# station_id = '6421500'
# observations = get_grdc_data(station_id, str(dstart), str(dend))
# obs_series = pd.Series(data = observations['streamflow'],
#                        index = observations.time.values)
obs_series = pd.read_csv('input/streamflow.txt', index_col = 0, header = None)
obs_series = pd.Series(data = obs_series[1],
                       index = obs_series.index)
# # Need to transform m3/s to mm/day
# cat_area = observations.grdc_catchment_area_in_km2 # in mk2
cat_area = 21301.0
obs_mm = (obs_series * 3.6 * 24) /cat_area
quality = pd.Series(data = obs_mm.values < 0, index = obs_mm.index)

# Let's calibrate

config_file = 'input/config.yml'
start_eval = '01 Jan 1992'

hbv = spotpy_setup(config_file = config_file,
                   observations = obs_mm,
                   start_eval = start_eval,
                   quality = quality,
                   debug = False)

# Calibration of the model -> it takes 15 hours. Uncomment only if needed
#sampler = spotpy.algorithms.sceua(hbv, dbname='C1, P1', dbformat='csv', alt_objfun = None)#, parallel = 'mpi')
#sampler.sample(repetitions = 5000)
#cal_pars = sampler.status.params # I don't think it is legal to do that but it works

cal_pars = [2.05832292, 1.04932726, -7.60513376,  2.41026983,  0.15561527, -7.74161426,  8.85941527,  6.93338587]

# Write the file
parameter_set['parameters']['s_max'] = float(np.exp(cal_pars[0]))
parameter_set['parameters']['beta'] = float(cal_pars[1])
parameter_set['parameters']['k_fr'] = float(np.exp(cal_pars[2]))
parameter_set['parameters']['alpha_fr'] = float(cal_pars[3])
parameter_set['parameters']['d_split'] = float(cal_pars[4])
parameter_set['parameters']['k_sr'] = float(np.exp(cal_pars[5]))
parameter_set['parameters']['tr'] = float(cal_pars[6])
parameter_set['parameters']['ce'] = float(cal_pars[7])
parameter_set['parameters']['m'] = float(1e-2)
parameter_set['init_states']['FR'] = float(0.0)
parameter_set['init_states']['SR'] = float(parameter_set['parameters']['d_split'] * np.mean(obs_mm.values[quality.values == 0])/parameter_set['parameters']['k_sr'])
parameter_set['init_states']['UR'] = float(0.8 * parameter_set['parameters']['s_max'])

# Save the updated file
with open('input/meuse_P1_calibrated.yml', 'w') as outfile:
    yaml.dump(data = parameter_set, stream = outfile)

# Simulate with BMI
from hbv.HBV_bmi import Bmi
config_file = 'input/meuse_P1_calibrated.yml'
bmi_model = Bmi()
bmi_model.initialize(config_file)
simulated = []
while bmi_model.get_current_time() < bmi_model.get_end_time():
    bmi_model.update()
    simulated.append(float(bmi_model.get_value(name = 'q_lag')))  # We can get also the other states if we need them
bmi_model.finalize()
simulated = pd.Series(data = simulated, index = obs_series.index)

fig, ax = plt.subplots(1, 1)
simulated.plot(ax = ax)
obs_mm.plot(ax = ax)