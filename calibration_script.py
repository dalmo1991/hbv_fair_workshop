from hbv.HBV_bmi_spotpy import spotpy_setup
import spotpy
import pandas as pd

#### CATCHMENT 1, PERIOD 1

# Initialize the model
config_file = 'config.yml'
start_eval = '01 Jan 1992'
observations = None #TODO

hbv = spotpy_setup(config_file = config_file,
                   observations = observations,
                   start_eval = start_eval)

# Calibration of the model
sampler = spotpy.algorithms.sceua(hbv, dbname='C1, P1', dbformat='csv')

results = sampler.sample(repetitions = 5000)