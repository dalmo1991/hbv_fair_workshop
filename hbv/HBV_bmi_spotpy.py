import spotpy
import numpy as np
import pandas as pd
from .HBV_bmi import Bmi
from .hbv_functions import define_weight
from grpc4bmi.bmi_client_docker import BmiClientDocker

class spotpy_setup(object):

    def __init__(self, config_file, observations, start_eval, quality = None, debug = True):
        
        self.params = [spotpy.parameter.Uniform('s_max', np.log(0.1), np.log(1000)), # 0
                       spotpy.parameter.Uniform('beta', 0.2, 5.0),                   # 1
                       spotpy.parameter.Uniform('k_fr', np.log(1e-8), np.log(1.0)),  # 2
                       spotpy.parameter.Uniform('alpha_fr', 1.0, 5.0),               # 3
                       spotpy.parameter.Uniform('d_split', 0.0, 1.0),                # 4
                       spotpy.parameter.Uniform('k_sr', np.log(1e-6), np.log(1e-2)), # 5
                       spotpy.parameter.Uniform('tr', 1.0, 10.0),                    # 6
                       spotpy.parameter.Uniform('ce', 0.1, 10)]                      # 7

#         self.bmi_model = BmiClientDocker(image='ewatercycle/hbv:latest', image_port=50001,
#                                          input_dir="./input",
#                                          output_dir="./output")
        self.config_file = config_file
        self.bmi_model = Bmi()
        self.bmi_model.initialize(config_file)
        self.observations = observations
        self.start_eval = start_eval
        self.dt = self.bmi_model.get_time_step()
        if quality is None:
            self.quality = pd.Series(data = [0]*len(observations),
                                     index = observations.index)
        else:
            self.quality = quality
        self.debug = debug
    
    def parameters(self):
        return spotpy.parameter.generate(self.params)
    
    def simulation(self, vector):
        # Not elegant but it works
        self.bmi_model.initialize(self.config_file)
        parameters = {'s_max' : np.exp(vector[0]),
                      'beta' : vector[1],
                      'k_fr' : np.exp(vector[2]),
                      'alpha_fr' : vector[3],
                      'd_split' : vector[4],
                      'k_sr' : np.exp(vector[5]),
                      'ce' : vector[7],
                      'm' : 1e-2,
                      'lag_weight' : define_weight(tr = vector[6], dt = self.dt)}
        
        quality_cut = self.quality.values
        
        states = {'UR' : 0.8 * np.exp(vector[0]),
                  'FR' : 0.0,
                  'SR' : parameters['d_split'] * np.mean(self.observations.values[quality_cut == 0])/parameters['k_sr'],
                  'lag_status' : [0] * len(parameters['lag_weight'])}
        if self.debug:
            print('Mean streamflow : {}'.format(np.mean(self.observations.values[quality_cut == 0])))
        # Set the new values of states and parameters
        for k in parameters.keys():
            if self.debug:
                print("Now setting: "+k+" as "+str(parameters[k]) + " of type " + str(type(parameters[k])))
            self.bmi_model.set_value(k, np.array([parameters[k]]))
        for k in states.keys():
            self.bmi_model.set_value(k, np.array([states[k]]))

        # Initialize the simulated streamflow
        simulated = []

        while self.bmi_model.get_current_time() < self.bmi_model.get_end_time():
            self.bmi_model.update()
            simulated.append(float(self.bmi_model.get_value(name = 'q_lag')))
            
        self.output = pd.Series(data = simulated,
                                index = self.observations.index)
        
        return [self.output.loc[self.start_eval:].values]
    
    def close(self):
        del self.bmi_model
        
    def evaluation(self):
        return self.observations.loc[self.start_eval:].values
    
    def objectivefunction(self,simulation,evaluation):
        """
        The objective function is Nash-Sutcliffe calculated with the square
        root of the streamflow
        """

        # Cut the quality array to the evaluation period
        quality_cut = self.quality.loc[self.start_eval:].values
        
        # Select only the data that have quality code equal to zero
        s = np.sqrt(simulation[0][quality_cut == 0])
        e = np.sqrt(evaluation[quality_cut == 0])
        obj_fun = spotpy.objectivefunctions.nashsutcliffe(evaluation = e,
                                                          simulation = s)
        
        if self.debug:
            print('NSE : {}'.format(obj_fun))
        return [obj_fun]