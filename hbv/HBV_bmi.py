from .hbv_functions import define_weight
from .hbv_functions import solve_hbv as model
from typing import Tuple
import pandas as pd
import numpy as np
import yaml
import xarray as xr

class Bmi(object):
    def initialize(self, config_file: str) -> None:

        with open(config_file, 'r') as file:
            config = yaml.load(file, Loader = yaml.FullLoader)

        self.S0 = config['init_states']
        self.parameters = config['parameters']
        forcings = config['forcings']
        self.states = dict(self.S0)

        # Define the weight function
        self.parameters['lag_weight'] = define_weight(self.parameters['tr'], self.parameters['dt'])
        self.states['lag_status'] = [0]*len(self.parameters['lag_weight'])
        
        self.dstart = config['start_date']
        self.dend = config['end_date']

        # Read the precipitation
        self.precipitation_pointer = xr.open_dataset(forcings['precipitation']).sel(time=pd.date_range(self.dstart, self.dend))

        # Read the PET
        self.pet_pointer = xr.open_dataset(forcings['potential_evapotranspiration']).sel(time=pd.date_range(self.dstart, self.dend))

        # Init output
        self.output = {'s_ur' : None,
                       'ea_ur' : None,
                       'q_ur' : None,
                       'q_fr' : None,
                       's_fr' : None,
                       'q_sr' : None,
                       's_sr' : None,
                       'q_lag' : None,
                       'lag_status' : None}

        self.time = 0.0
        self.time_step_string = config['time_step_unit']

    def update(self, time_interval = None) -> None:
        if time_interval is not None:
            raise ValueError('Time interval is set in the configuration file')

        internal_time_index = int(np.round(self.time))

        # We assume that precipitation and pet are positive. If not we set them to zero
        # Precipitation and pet are given in meters. Here we transform to mm
        precipitation = max(0, float(self.precipitation_pointer.tp[0][internal_time_index]))*1000
        pet = max(0, float(self.pet_pointer.pev[0][internal_time_index]))*1000

        self.output = model(p = precipitation, ep = pet, pars = self.parameters,
                            s0 = self.states, dt = self.parameters['dt'])

        # Update the state
        self.states['UR'] = self.output['s_ur']
        self.states['FR'] = self.output['s_fr']
        self.states['SR'] = self.output['s_sr']
        self.states['lag_status'] = self.output['lag_status']

        self.time += 1

    def finalize(self) -> None:
        self.precipitation_pointer.close()
        self.pet_pointer.close()

    def get_component_name(self) -> str:
        return 'HBV'

    def get_input_var_names(self) -> Tuple[str]:
        return ()

    def get_output_var_names(self) -> Tuple[str]:
        return tuple(self.output.keys())

    def get_var_grid(self, name: str) -> int:
        return 0

    def get_var_type(self, name: str) -> str:
        return 'float'

    def get_var_units(self, name: str) -> str:
        if name in ['s_ur', 's_sr', 's_fr', 'lag_status']:
            return 'mm'
        elif name in ['q_ur', 'q_sr', 'q_fr', 'q_lag', 'ea_ur']:
            return 'mm/{}'.format(self.get_time_units())
        else:
            raise ValueError('The variable does not exist')

    def get_var_itemsize(self, name: str) -> int:
        return 8

    def get_var_nbytes(self, name: str) -> int:
        if name in ['s_ur', 's_sr', 's_fr']:
            return 8
        elif name in ['q_ur', 'q_sr', 'q_fr', 'q_lag', 'ea_ur']:
            return 8
        elif name == 'lag_status':
            return 8*len(self.states['lag_status'])
        else:
            raise ValueError('The variable does not exist')

    def get_var_location(self, name: str) -> str:
        return 'node'

    def get_current_time(self) -> float:
        return float(self.time)

    def get_start_time(self) -> float:
        return 0.0

    def get_end_time(self) -> float:
        return float(self.precipitation_pointer.dims['time'])

    def get_time_units(self) -> str:
        return self.time_step_string

    def get_time_step(self) -> float:
        return self.parameters['dt']

    def get_value(self, name: str) -> np.ndarray:
        return np.array(self.output[name])

    def get_value_ptr(self, name: str) -> np.ndarray:
        raise NotImplementedError('This is not used in this configration')

    def get_value_at_indices(self, name: str, inds: np.ndarray) -> np.ndarray:
        if not (inds == [0, 0]).all():
            raise ValueError('The inds can only be (0, 0)')

        return np.array(self.output[name])

    def set_value(self, name: str, values: np.ndarray) -> None:
#         if len(values) != 1:
#             raise ValueError('values must contain only one value')
        if name in ['lag_status']:
            self.states[name] = values.flatten()
        elif name in self.parameters.keys():
            self.parameters[name] = values.flat[0]
        elif name in self.states.keys():
            self.states[name] = values.flat[0]
        else:
            raise ValueError('{} can not be set'.format(name))

    def set_value_at_indices(self, name: str, inds: np.ndarray, src: np.ndarray) -> None:
        raise NotImplementedError('This is not used in this configration')

    # Grid information
    def get_grid_rank(self, grid: int) -> int:
        return 2

    def get_grid_size(self, grid: int) -> int:
        return 1

    def get_grid_type(self, grid: int) -> str:
        return 'uniform_rectilinear'

    # Uniform rectilinear
    def get_grid_shape(self, grid: int) -> np.ndarray:
        return np.array((1, 1))

    def get_grid_spacing(self, grid: int) -> np.ndarray:
        return np.array([0, 0])

    def get_grid_origin(self, grid: int) -> np.ndarray:
        return np.array((self.parameters['lat'], self.parameters['lon']))

    # Non-uniform rectilinear, curvilinear
    def get_grid_x(self, grid: int, x: np.ndarray) -> None:
        raise NotImplementedError('This is not used in this configuration')

    def get_grid_y(self, grid: int, y: np.ndarray) -> None:
        raise NotImplementedError('This is not used in this configuration')

    def get_grid_z(self, grid: int, z: np.ndarray) -> None:
        raise NotImplementedError('This is not used in this configuration')

    def get_grid_node_count(self, grid: int) -> int:
        raise NotImplementedError('This is not used in this configuration')

    def get_grid_edge_count(self, grid: int) -> int:
        raise NotImplementedError('This is not used in this configuration')

    def get_grid_face_count(self, grid: int) -> int:
        raise NotImplementedError('This is not used in this configuration')

    def get_grid_edge_nodes(self, grid: int, edge_nodes: np.ndarray) -> None:
        raise NotImplementedError('This is not used in this configuration')

    def get_grid_face_edges(self, grid: int, face_edges: np.ndarray) -> None:
        raise NotImplementedError('This is not used in this configuration')

    def get_grid_face_nodes(self, grid: int, face_nodes: np.ndarray) -> None:
        raise NotImplementedError('This is not used in this configuration')

    def get_grid_nodes_per_face(self, grid: int, nodes_per_face: np.ndarray) -> None:
        raise NotImplementedError('This is not used in this configuration')
