from scipy.optimize import root_scalar
import numpy as np
import pandas as pd

debug = False

def solve_UR(p, ep, pars, s0, dt):
    """
    This function solves the unsaturated reservoir.

    Parameters:
    p : float
        Precipitation input
    ep : float
        Potential evapotranspiration
    pars : dict
        Parameters of the reservoir. It must contain:
            's_max' : maximum capacity
            'm' : shape parameter governing the evapotranspiration
            'beta' : exponent of the power law governing the outflow
            'ce' : multiplier for the evapotranspiration
    s0 : float
        Initial state of the reservoir
    dt : float
        Time step

    Returns:
    dict
        Outputs of the reservoir:
            'ea_ur' : actual evapotranspiration from the reservoir
            'q_ur' : outflow from the reservoir
            's_ur' : storage at the end of the time step
    """

    out = dict()
    p = float(p)
    ep = float(ep)

    # Calculate S(t+1)
    try:
        out['s_ur'] = root_scalar(f = lambda S_new : S_new - s0 - dt*(p - p*(S_new/pars['s_max'])**pars['beta']-
                                    pars['ce'] * ep * (S_new/pars['s_max']) * (1+pars['m'])/((S_new/pars['s_max']) + pars['m'])),
                                method = 'brentq', bracket = [0.0, s0 + p],
                                xtol = 1e-10).root
        # Calculate the fluxes
        s_bar = out['s_ur']/pars['s_max']
        out['ea_ur'] = pars['ce'] * ep*s_bar*(1 + pars['m'])/(s_bar + pars['m'])
        out['q_ur'] = p * s_bar ** pars['beta']
    except ValueError as ve:
        if debug:
            print('Function solve_UR, root_scalar did not work')
            print(ve)
            print('p : {}'.format(p))
            print('ep : {}'.format(ep))
            print('s_max : {}'.format(pars['s_max']))
            print('m : {}'.format(pars['m']))
            print('beta : {}'.format(pars['beta']))
            print('ce : {}'.format(pars['ce']))
            print('s0 : {}'.format(s0))
        out['s_ur'] = s0 + p
        out['ea_ur'] = 0
        out['q_ur'] = 0
    
    # wb = out['s_ur'] - s0 - p + out['ea_ur'] + out['q_ur']
    return out

def solve_FR(p, pars, s0, dt):
    """
    This function solves the fast reservoir.

    Parameters:
    p : float
        Precipitation input
    pars : dict
        Parameters of the reservoir. It must contain:
            'd_split' : split between fast and slow reservoir (1 means that all the water goes to the slow reservoir)
            'k_fr' : multiplier of the power law governing the outflow
            'alpha_fr' : exponent of the power law governing the outflow
    s0 : float
        Initial state of the reservoir
    dt : float
        Time step

    Returns:
    dict
        Outputs of the reservoir:
            'q_fr' : outflow from the reservoir
            's_fr' : storage at the end of the time step
    """

    p_in = float((1 - pars['d_split'])*p)

    out = dict()

    try:
        # Calculate S(t+1)
        out['s_fr'] = root_scalar(f = lambda S_new : S_new - s0 - dt*(p_in - pars['k_fr'] * (S_new ** pars['alpha_fr'])),
                                method = 'brentq', bracket = [0.0, s0 + p_in],
                                xtol = 1e-10).root

        # Calculate the fluxes
        out['q_fr'] = pars['k_fr'] * (out['s_fr'] ** pars['alpha_fr'])
    except ValueError as ve:
        if debug:
            print('Function solve_FR, root_scalar did not work')
            print(ve)
            print('p : {}'.format(p_in))
            print('d_split : {}'.format(pars['d_split']))
            print('k_fr : {}'.format(pars['k_fr']))
            print('alpha_fr : {}'.format(pars['alpha_fr']))
            print('s0 : {}'.format(s0))
        out['s_fr'] = s0 + p_in
        out['q_fr'] = 0
#    wb = out['s_fr'] - s0 - p_in + out['q_fr']

    return out

def solve_SR(p, pars, s0, dt):
    """
    This function solves the slow reservoir.

    Parameters:
    p : float
        Precipitation input
    pars : dict
        Parameters of the reservoir. It must contain:
            'd_split' : split between fast and slow reservoir (1 means that all the water goes to the slow reservoir)
            'k_sr' : multiplier of the power law governing the outflow
    s0 : float
        Initial state of the reservoir
    dt : float
        Time step

    Returns:
    dict
        Outputs of the reservoir:
            'q_sr' : outflow from the reservoir
            's_sr' : storage at the end of the time step
    """

    p_in = float((pars['d_split'])*p)

    out = dict()

    try:
        # Calculate S(t+1)
        out['s_sr'] = root_scalar(f = lambda S_new : S_new - s0 - dt*(p_in - pars['k_sr'] * S_new),
                                method = 'brentq', bracket = [0.0, s0 + p_in],
                                xtol = 1e-10).root

        # Calculate the fluxes
        out['q_sr'] = pars['k_sr'] * out['s_sr']
    except ValueError as ve:
        if debug:
            print('Function solve_SR, root_scalar did not work')
            print(ve)
            print('p : {}'.format(p_in))
            print('d_split : {}'.format(pars['d_split']))
            print('k_sr : {}'.format(pars['k_sr']))
            print('s0 : {}'.format(s0))
        out['s_sr'] = s0 + p_in
        out['q_sr'] = 0
#    wb = out['s_sr'] - s0 - p_in + out['q_sr']

    return out

def define_weight(tr, dt):
    """
    This function defines the weight for the lag function

    Parameters:
    tr : float
        Rise time of the lag function
    dt : float
        Time step

    Returns
    list
        Weight vector for the lag function
    """

    w = [0] * int(np.ceil(2*tr/dt))

    if 2*tr < dt:
        w[0] = 1
    else:
        for i in range(len(w)):
            if i < np.floor(tr/dt):
                w[i] = (2*i + 1)*(dt**2)/(2*(tr**2))
            if i == np.floor(2*tr/dt):
                w[i] = ((2*tr - i*dt)**2)/(2*(tr**2))
            if (i >= np.ceil(tr/dt)) and (i < np.floor(2*tr/dt)):
                w[i] = ((4*tr - (2*i + 1)*dt)*dt)/(2*(tr**2))
        if np.sum(w) < 1-1e-7: # some tollerance in case the sum is not exactly 1
            w[int(np.floor(tr/dt))] = 1 - np.sum(w)

    return w


def solve_lag(q_in, pars, status):
    """
    This function solves the lag function

    Parameters
    q_in : float
        Streamflow input
    pars : dict
        Parameters of the lag function. It must contain:
            'lag_weight' : weight vector. It can be initialized using the 'define_weight' function
    status : list
        Initial state of the lag function

    Returns:
    dict
        Outputs of the reservoir:
            'q_lag' : outflow from the lag function
            'lag_status' : new status of the lag function
    """
    
    new_status = np.array(status) + q_in * np.array(pars['lag_weight'])

    out = dict()
    out['q_lag'] = float(new_status[0])
    out['lag_status'] = new_status[1:].tolist()
    out['lag_status'].append(0)

    return out

def solve_hbv(p, ep, pars, s0, dt):
    """
    This function solves the HBV model for a time-series of inputs

    Parameters:
    p : float
        Precipitation input
    ep : float
        Potential evapotranspiration
    pars : dict
        Parameters of all the reservoirs
    s0 : dict
        Initial state of all the reservoir. The keys are:
            'UR' : unsaturated reservoir
            'FR' : fast reservoir
            'SR' : slow reservoir
            'lag_status' : lag function
    dt : float
        Time step

    Returns:
    dict
        Outputs and states of the reservoirs.
    """

    # Solve UR
    out_UR = solve_UR(p = p, ep = ep, pars = pars, s0 = s0['UR'], dt = dt)
    # Solve FR
    out_FR = solve_FR(p = out_UR['q_ur'], pars = pars, s0 = s0['FR'], dt = dt)
    # Solve SR
    out_SR = solve_SR(p = out_UR['q_ur'], pars = pars, s0 = s0['SR'], dt = dt)
    # Solve lag
    out_lag = solve_lag(q_in = out_FR['q_fr'] + out_SR['q_sr'], pars = pars, status = s0['lag_status'])

    output = {**out_UR, **out_FR, **out_SR, **out_lag}

    return output

if __name__ == '__main__':
    # NOTE: this main file doesn't work anymore but is kept to have a reference
    # 01 - Read the input data
#    precipitation = np.loadtxt('precipitation.csv')
#    pet = np.loadtxt('pet.csv')
    precipitation = np.random.rand(10)*10
    pet = np.random.rand(10)*2

    # 02 - Set the values of the parameters and the initial storage
    pars = dict()
    # UR
    pars['s_max'] = 15.0
    pars['m'] = 1.0
    pars['beta'] = 2.0
    pars['ce'] = 1.0
    # FR
    pars['d_split'] = 0.3
    pars['k_fr'] = 0.01
    pars['alpha_fr'] = 2.0
    # SR
    pars['k_sr'] = 1e-1
    # lag
    tr = 2
    dt = 1
    pars['lag_weight'] = define_weight(tr = tr, dt = dt)

    # Initial states
    s0 = dict()
    s0['UR'] = 10
    s0['FR'] = 5.0
    s0['SR'] = 5.0
    s0['lag_status'] = [0]*len(pars['lag_weight'])

    out = solve_hbv(p = precipitation[0], ep = pet[0], pars = pars, s0 = s0, dt = dt)