import spotpy
from .hbv_functions import solve_hbv as hydrological_model
from .hbv_functions import define_weight
import numpy as np
import pandas as pd

class spotpy_setup(object):

    def __init__(self, precipitation, pet, start_eval, dt, observations,
                 quality = None):
        """
        This class wraps the hbv code to use it with spotpy.

        Parameters
        ----------
        precipitation : pd.Series
            Time series of precipitation. Index must be a timestamp. It has to
            be cut to the right period, including the warm-up.
        pet : pd.Series
            Time series of potential evapotranspiration. Index must be a 
            timestamp. It has to be cut to the right period, including the 
            warm-up.
        start_eval : str
            String interpretable by pandas that encodes the starting date of the
            evaluation period.
        dt : float
            Time step used by the model.
        observations : pd.Series
            Time series of observed streamflow. Index must be a timestamp. It 
            has to be cut to the right period, including the warm-up.
        quality : pd.Series
            Time series of quality codes. 0 means that the data are good, 1 that 
            they are wrong. Index must be a timestamp. It has to be cut to the 
            right period, including the warm-up. If None it means that the data
            is ok.
        """
        
        raise DeprecationWarning('Don not use this. Use the Bmi version')
        self.parameters = [spotpy.parameter.Uniform('s_max', np.log(0.1), np.log(1000)), # 0
                           spotpy.parameter.Uniform('beta', 0.2, 5.0),                   # 1
                           spotpy.parameter.Uniform('k_fr', np.log(1e-8), np.log(1.0)),  # 2
                           spotpy.parameter.Uniform('alpha_fr', 1.0, 5.0),               # 3
                           spotpy.parameter.Uniform('d_split', 0.0, 1.0),                # 4
                           spotpy.parameter.Uniform('k_sr', np.log(1e-6), np.log(1e-2)), # 5
                           spotpy.parameter.Uniform('tr', 1.0, 10.0),                    # 6
                           spotpy.parameter.Uniform('ce', 0.1, 10)]                      # 7

        precipitation[precipitation < 0] = 0.0
        pet[pet < 0] = 0.0
        self.precipitation = precipitation
        self.pet = pet
        self.start_eval = start_eval
        self.dt = dt
        self.observations = observations
        if quality is None:
            self.quality = pd.Series(data = [0]*len(observations),
                                     index = observations.index)
        else:
            self.quality = quality
    
    def parameters(self):
        return spotpy.parameter.generate(self.parameters)
    
    def simulation(self, vector):
        
        parameters = {'s_max' : np.exp(vector[0]),
                      'beta' : vector[1],
                      'k_fr' : np.exp(vector[2]),
                      'alpha_fr' : vector[3],
                      'd_split' : vector[4],
                      'k_sr' : np.exp(vector[5]),
                      'ce' : vector[7],
                      'm' : 1e-2,
                      'lag_weight' : define_weight(tr = vector[6], dt = self.dt)}
        
        states = {'UR' : 0.8 * np.exp(vector[0]),
                  'FR' : 0.0,
                  'SR' : parameters['d_split'] * np.mean(self.observations.values)/parameters['k_sr'],
                  'lag_status' : [0] * len(parameters['lag_weight'])}
        
        prec_local = self.precipitation.values
        pet_local = self.pet.values
        self.output = []
        
        for p, pet in zip(prec_local, pet_local):
            self.output.append(hydrological_model(p = p, ep = pet, pars = parameters,
                                             s0 = states, dt = self.dt))
            
            # Update the states
            states['UR'] = self.output[-1]['s_ur']
            states['FR'] = self.output[-1]['s_fr']
            states['SR'] = self.output[-1]['s_sr']
            states['lag_status'] = self.output[-1]['lag_status']
            
        self.output = pd.DataFrame(self.output)
        self.output.index = self.precipitation.index
        
        return self.output.loc[self.start_eval:, 'q_lag'].values
        
    def evaluation(self):
        return self.observations.loc[self.start_eval:].values
    
    def objectivefunction(self,simulation,evaluation):
        """
        The objective function is Nash-Sutcliffe calculated with the square
        root of the streamflow
        """

        # Cut the quality array to the evaluation period
        quality_cut = self.quality.loc[self.start_eval:].values
        
        # Select only the data that have quality code equal to zero
        s = np.sqrt(simulation[quality_cut == 0])
        e = np.sqrt(evaluation[quality_cut == 0])
        obj_fun = spotpy.objectivefunctions.nashsutcliffe(evaluation = e,
                                                          simulation = s)
        
        return obj_fun