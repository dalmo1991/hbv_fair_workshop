FROM python:3

RUN pip3 install grpc4bmi

RUN apt-get update && apt-get install -y libnetcdf-dev

COPY . /opt/hbv_bmi
WORKDIR /opt/hbv_bmi
RUN pip3 install .


ENV BMI_MODULE=hbv.HBV_bmi
ENV BMI_CLASS=Bmi

EXPOSE 50001

ENTRYPOINT run-bmi-server --lang python --port 50001