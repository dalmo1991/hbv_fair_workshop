from __future__ import absolute_import, print_function

import io
import os

from setuptools import find_packages, setup

setup(
    name='hbv',
    version='1.0.4',
    license='Apache Software License',
    author='Marco Dal Molin',
    install_requires=[
        'numpy',
        'pyyaml',
        'scipy',
        'pandas',
        'xarray',
        'netcdf4',
        'spotpy',
    ],
    author_email='marco.dalmolin@eawag.ch',
    description=(
        ''
    ),
    long_description='',
    packages=find_packages(),
    include_package_data=True,
    platforms='any',
    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Development Status :: 3 - Alpha',
        'Natural Language :: English',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: OS Independent',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
)
